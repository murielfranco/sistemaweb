<?php

include_once "includes/url_dispacher.php";
include_once "includes/template_renderer.php";
include_once "includes/cache.php";

$_CACHE = new Cache();

$dispacher = new URLDispacher(new URLPatterns([
		
		"/^\/$/" => "frontend/templates/home.php",
		"/^\/home\/$/" => "frontend/templates/home.php",
		"/^\/station\/(?<station_id>\d+)\/$/" => "frontend/templates/station.php"
		
	]),"/^\/imanust/","frontend/templates/404.php");

$match = $dispacher->match($_SERVER["SCRIPT_URL"]);

if($match->found == FALSE)
{
	header("HTTP/1.0 404 Not Found");
}

if($match->include)
{
	$_DATA = $match->data;
	include $match->include;
}

?>
