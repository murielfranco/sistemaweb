<?php

/*
 * MySQL settings
 * 
 * Please, do not commit changes to this file unless you're defining a
 * new constant.
 * 
 */
 
/* The name of the database */
define('DB_NAME', 'testedb');

/* MySQL database username */
define('DB_USER', 'root');

/* MySQL database password */
define('DB_PASSWORD', 'gremio');

/* MySQL hostname */
define('DB_HOST', 'localhost');

/* Password hash function */
define('PSW_HASH_FUNC', 'sha256');

if(!file_exists("local__config.php"))
{
	copy("default__config.php","local__config.php");
}

include_once "local__config.php"

?>
