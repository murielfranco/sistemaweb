<?php

//Doesn't make much sense, but works for now I guess...

class Rendered
{
	private $template;
	private $default_context;
	
	public function __construct($template)
	{
		ob_start();
		
		include $template;
		
		$this->template = ob_get_contents();
		$this->default_context = $default_template_context;
		
		ob_end_clean();
	}
	
	public function render($context=[])
	{
		$rendered = $this->template;
		
		foreach($this->default_context as $key => $value)
		{
			if(array_key_exists($key,$context))
			{
				$value = $context[$key];
			}
			
			$rendered = str_replace("%".$key."%",$value,$rendered);
		}
		
		return $rendered;
	}
}

?>
