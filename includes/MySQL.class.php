<?php

class MySQL {
    private $host;
    private $user;
    private $password;
    private $db_name;
    private $link;
    private $query;
    
    function __construct($hostname, $username, $password, $db_name) {
        $this->host = $hostname;
        $this->user = $username;
        $this->password = $password;
        $this->db_name = $db_name;
        
        $this->connect();
    }
    
    function __destruct() {
        $this->close();
    }
    
    private function connect() {
        /* trying to connect to mysql */
        if ($this->link = mysql_connect($this->host, $this->user, $this->password)) {
            /* trying to select db_name */
            if (!mysql_select_db($this->db_name, $this->link)) {
                throw new Exception("Could not connect to \"" . $this->db_name . "\".");
            }
        } else {
            throw new Exception('Could not create database connection.');
        }
    }
    
    public function query($sql_query) {
        $result = mysql_query($sql_query, $this->link);
        if (!$result) {
            throw new Exception('Invalid query: ' . mysql_error());
        }

        return $result;
    }

    public function fetch_array($result) {
        try {
            return mysql_fetch_array($result);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
    
    public function close() {
        @mysql_close($this->connection);
    }
}

?>
