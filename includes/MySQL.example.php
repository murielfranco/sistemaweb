<?php

include('../config.php');
include('MySQL.class.php');

try {
    $db = new MySQL(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

    /* Create table example */
    //$res = $db->query("CREATE TABLE test_table (id INT, data VARCHAR(30));");

    /* Insert into table example */
    //$res = $db->query("INSERT INTO test_table (id, data) VALUES (42, 'lala');");

    /* List items from table example */
    $res = $db->query("SELECT * FROM test_table;");
    while ($row = $db->fetch_array($res)) {
            printf("ID: %s DATA: %s", $row[0], $row[1]);  
        }
    
} catch (Exception $e) {
    echo $e->getMessage();
}


?>
