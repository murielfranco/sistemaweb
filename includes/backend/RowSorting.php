<?php

class RowSorting
{
	public $row;
	public $field;
	
	public function __construct($objects_row, $field_to_compare)
	{
		$this->row = $objects_row;
		$this->field = $field_to_compare;
	}
	
	public function __toString()
	{
		$ret = "";
		
		foreach($this->row as $r)
		{
			$ret = $ret . $r . " ";
		}
		
		return $ret;
	}
	
	public static function __compareTo($a, $b)
	{
		return $a->row[$a->field]->compareTo($b->row[$b->field]);
	}
}

?>
