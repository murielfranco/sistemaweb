<?php

/**
 * 
 * Exception thrown when someone tries to add a row with the wrong number of fields.
 *
 */

class RowLengthMismatch extends Exception
{
	private $received;
	private $expected;
	
	public function __construct($r, $e)
	{
		$this->received = $r;
		$this->expected = $e;
	}
	
	public function __toString()
	{
		return "Expected " . $this->expected . " values. Received " . $this->received . ".";
	}
}

?>
