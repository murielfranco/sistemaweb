<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of newPHPClass
 *
 * @author Luciano
 */

include_once "third/Unidecode.php";
include_once "BaseCell.php";
include_once "TableData.php";
include_once "ContentNotParseable.php";

class ClimateParser implements ParserInterface
{
    public function parse($content)
    {
		$table = new TableData();
		$lines = array();
		$columns = array();
		$classes = array();
		$i=0;
		$j=0;
		
		$content = rtrim($content);
		
		foreach(explode("\n", $content) as $s)
		{
			$line = array();
			
			foreach(explode("\t", $s) as $s2)
			{
				$line[]  = $s2;
			}
			
			if(sizeof($lines)>0 && sizeof($line) != sizeof($lines[0]))
			{
				throw new ContentNotParseable();
			}
			
			$lines[] = $line;
		}
		
		if(sizeof($lines)<3)
		{
			throw new ContentNotParseable();
		}
		
		for($i=0; $i < sizeof($lines[0]); $i++)
		{
			$column_name;
			
			if(sizeof(trim($lines[0][$i]))>0)
			{
				$column_name = trim($lines[0][$i]) . " " . trim($lines[1][$i]);
			}
			else
			{
				$column_name = trim($lines[1][$i]);
			}
			
			$c = "BaseCell";
			
			if(preg_match("/^\d+([\.,]\d+)?$/",$lines[2][$i]))
			{
				$c = "BaseCellDecimal";
			}
			else if(preg_match("/^\d{2}\/\d{2}\/\d{2}$/",$lines[2][$i]))
			{
				$c = "BaseCellDate";
			}
			else if(preg_match("/^\d{1,2}:\d{2}\s[ap]$/",$lines[2][$i]))
			{
				$c = "BaseCellTime";
			}
			
			$columns[] = $column_name;
			$classes[] = $c;
		}
		
		for($i=0; $i < sizeof($columns); $i++)
		{
			$column_name = $columns[$i];
			$c = $classes[$i];
			
			if($i+1 < sizeof($columns) && $c == "BaseCellDate" && $classes[$i+1] == "BaseCellTime")
			{
				$column_name = $column_name . " " . $columns[$i+1];
				$c = "BaseCellDateTime";
				
				array_splice($classes, $i, 1);
				$classes[$i] = $c;
				
				array_splice($columns, $i, 1);
				$columns[$i] = $column_name;
			}
			
			try
			{
				$table->addColumn($column_name,$c);
			}
			catch(ColumnAlreadyExists $e)
			{
				for($j=2;;$j++)
				{
					$neo_column_name = $column_name . " " . $j;
					
					try
					{
						$table->addColumn($neo_column_name,$c);
						
						break;
					}
					catch(ColumnAlreadyExists $e2)
					{
					}
				}
			}
		}
		
		for($i=2; $i<sizeof($lines); $i++)
		{
			$row = array();
			
			$diff = 0;
			
			for($j=0; $j<sizeof($lines[$i]); $j++)
			{
				$o = $lines[$i][$j];
				$c = $classes[$j + $diff];
				
				if($c == "BaseCellDecimal")
				{
					$o = new BaseCellDecimal(str_replace(",", ".", $o));
				}
				else if($c == "BaseCellDate")
				{
					$temp = explode("/", $o);
					
					$o = BaseCellDate::createFromSpecific(
						intval($temp[2]) + 2000,
						intval($temp[1]),
						intval($temp[0])
					);
				}
				else if($c == "BaseCellTime")
				{
					$temp = preg_split("/[\s:]/",$o);
					$hour = intval($temp[0]);
					
					if($hour == 12 && $temp[2] == "a")
					{
						$hour = 0;
					}
					else if($hour < 12 && $temp[2] == "p")
					{
						$hour += 12;
					}
					
					var_dump($temp);
					
					$o = BaseCellTime::createFromSpecific(
						$hour,
						$temp[1],
						0
					);
				}
				else if($c == "BaseCellDateTime")
				{
					$tempDate = explode("/", $o);
					$tempTime = preg_split("/[\s:]/",$lines[$i][$j+1]);
					$hour = intval($tempTime[0]);
					
					if($hour == 12 && $tempTime[2] == "a")
					{
						$hour = 0;
					}
					else if($hour < 12 && $tempTime[2] == "p")
					{
						$hour += 12;
					}
					
					$o = BaseCellDateTime::createFromSpecific(
						intval($tempDate[2]) + 2000,
						intval($tempDate[1]),
						intval($tempDate[0]),
						$hour,
						$tempTime[1],
						0
					);
					
					$j++;
					$diff--;
				}
                else
                {
					$o = new BaseCell($o);
				}
				
				$row[] = $o;
			}
			
			try
			{
				$table->addRow($row);
			}
			catch(TypeMismatch $e)
			{
				echo $e;
				
				throw new ContentNotParseable();
			}
			catch(RowLengthMismatch $e)
			{
				echo $e;
				
				throw new ContentNotParseable();
			}
		}
		
		
        return $table;
    }
}
/*
$p = new ClimateParser();
$file = fopen("../../local/EHM02_31102013.txt","r");
$contents = fread($file,filesize("../../local/EHM02_31102013.txt"));
$contents = mb_convert_encoding($contents,"UTF-8","ISO-8859-1");
fclose($file);

var_dump($p->parse($contents))
*/
?>
