<?php

class BaseCellTime extends BaseCell
{
	private $hour;
	private $minute;
	private $second;
	
	public function __construct($rawInfo)
	{
		parent::__construct($rawInfo);
		
		$tempDate = explode(":", $rawInfo);
		
		$this->hour = $tempDate[0];
		$this->minute = $tempDate[1];
		$this->second = $tempDate[2];
	}
	
	public static function createFromSpecific($hour, $minute=0, $second=0)
	{
		return new BaseCellTime($hour.":".$minute.":".$second);
	}
	
	public function compareTo($that)
	{
		if($this->hour == $that->hour)
		{
			if($this->minute == $that->minute)
			{
				if($this->second == $that->second)
				{
					return 0;
				}
				
				return ($this->second < $that->second) ? -1 : 1;
			}
			
			return ($this->minute < $that->minute) ? -1 : 1;
		}
		
		return ($this->hour < $that->hour) ? -1 : 1;
	}
}

?>
