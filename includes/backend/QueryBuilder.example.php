<?php
/*
 * Este arquivo demonstra como inserir uma TableData no MySQL utilizando 
 * o QueyBuilder pra gerar as Queries.
 * 
 */ 

include('../../config.php');

include('../MySQL.class.php');
include('TableData.php');
include('QueryBuilder.php');

$t = new TableData();
$t->addColumn("Date","BaseCell");
$t->addColumn("Date2","BaseCell");

$t->addRow(array(new BaseCell("123"), new BaseCell("4")));
$t->addRow(array(new BaseCell("123"), new BaseCell("2")));
$t->addRow(array(new BaseCell("123"), new BaseCell("3")));

$t->setInfoField("Estacao","Jujuba");

$builder = new QueryBuilder($t);

$db = new MySQL(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

/* Inserting a Estacao */
//$db->query($builder->insertStation());

/* Inserir coluna */
//$db->query ($builder->insertColumns());

//$db->close();


?>
