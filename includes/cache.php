<?php

class Cache
{
	private $basedir;
	private $name;
	private $time;
	
	public function __construct($basedir="/tmp/php_cache/")
	{
		$this->basedir = $basedir;
		
		if (!file_exists($this->basedir))
		{
			mkdir($this->basedir, 0777, true);
		}
	}
	
	public function start($name, $time)
	{
		$this->name = str_replace("/","_",$name);
		$this->time = $time;
		ob_start();
	}
	
	public function finish()
	{
		$now = date_create("now",new DateTimeZone("UTC"));
		$leave = date_add($now,new DateInterval("PT".$this->time."S"));
		
		$filename = $this->basedir.$this->name;
		$f = fopen($filename, "w");
		fwrite($f,$leave->format("Y-m-d H:i:s")."\n");
		fwrite($f,ob_get_contents());
		fclose($f);
		
		unset($this->name);
		unset($this->time);
	}
	
	public function get_cached($name)
	{
		$filename = $this->basedir.$name;
		
		if(!file_exists($filename))
		{
			return NULL;
		}
		
		$f = fopen($filename, "r");
		
		$time = fread($f,strlen("YYYY-MM-DD HH:MM:SS"));
		$time = date_create($time,new DateTimeZone("UTC"));
		$now = date_create("now",new DateTimeZone("UTC"));
		
		if($now > $time)
		{
			fclose($f);
			
			unlink($filename);
			
			return NULL;
		}
		
		fgetc($f);
		
		$content = fread($f,filesize($filename));
		fclose($f);
		return $content;
	}
	
	public function get_cached_and_die($name)
	{
		$content = $this->get_cached($name);
		
		if($content)
		{
			die($content);
		}
	}
	
	public function get_cached_and_die_or_start($name,$time)
	{
		$content = $this->get_cached($name);
		
		if($content)
		{
			die($content);
		}
		else
		{
			$this->start($name,$time);
		}
	}
	
	public function clean_output_buffer()
	{
		ob_end_clean();
	}
}
