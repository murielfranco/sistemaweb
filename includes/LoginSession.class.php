<?php
ob_start();
session_start();

include_once('config.php');
include_once('MySQL.class.php');
include_once('backend/QueryBuilder.php');

class LoginSession {
	private $db;
	private $queryBuilder;

	function __construct() {
		$this->db = new MySQL(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
		$this->queryBuilder = new QueryBuilder(null);
	}

	public function login($email, $password) {
		$query = $this->queryBuilder->buildLoginQuery($email);
		
		$result = $this->db->query($query);
		if ($this->userNotFound($result)) {
			throw new Exception("User not found");
		} else {
			$userData = $this->db->fetch_array($result);
			// check whether password is valid
			if ($this->authUser($password, $userData)) {
				// create login session 
				session_regenerate_id();
				$_SESSION['session_user_email'] = $userData['email'];
				session_write_close();
			} else {
				throw new Exception("Invalid password");
			}
		}
	}

	public function hashPassword($password) {
		return hash(PSW_HASH_FUNC, $password);
	}

	private function userNotFound($result) {
		return (mysql_num_rows($result) == 0);
	}

	private function authUser($password, $userData) {
		return (hash(PSW_HASH_FUNC, $password) == $userData['senha']);
	}

	public function logout() {
		unset($_SESSION['session_user_email']);
	}

	public function is_authenticated() {
		return isset($_SESSION['session_user_email']);
	}
}

?>
