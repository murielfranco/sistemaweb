<?php

require_once('includes/User.class.php');

if (isset($_POST['email']) && isset($_POST['password'])) {
    try {
        $user = new User($_POST['email']);
        $user->login($_POST['password']);

        if ($user->is_authenticated()) {
            header('Location: login_form.php');
        }
    } catch (Exception $e) {
        header('Location: login_form.php?error='. $e->getMessage());
    }
}

?>