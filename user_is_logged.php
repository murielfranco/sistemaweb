<?php

/*
 * Este arquivo deve ser incluído em todas as seções de código que se deseja 
 * verificar a identidade de um usuário.
 *
 */

session_start();

include('includes/User.class.php');

$user = new AnonymousUser();

if (isset($_SESSION['session_user_email'])) {
    $user = new User($_SESSION['session_user_email']);
}

?>