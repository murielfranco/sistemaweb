<?php

//$_CACHE->get_cached_and_die_or_start("Home",3600);

$renderer = new Rendered("frontend/templates/base.php");

ob_start();

?>

<link rel="stylesheet" href="/static/css/home.css" type="text/css"/>

<?php

$css = ob_get_contents();
ob_end_clean();

ob_start();

?>

<div class="home">
	<img src="/static/img/home.jpg"/>
	<p>
		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus dignissim suscipit gravida. Praesent mollis justo vel elit rutrum laoreet. Donec cursus laoreet est et pharetra. Quisque vel orci viverra diam vestibulum euismod. Praesent tristique porttitor lacus, quis lacinia dolor tempus a. Nulla semper erat luctus massa egestas fringilla sit amet sed mauris. Morbi rhoncus turpis et felis volutpat scelerisque. Suspendisse accumsan ornare erat ac lobortis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Proin sit amet magna condimentum justo fermentum iaculis eu at diam. Donec porttitor dapibus neque, eget scelerisque dui tincidunt eu. Donec lacinia eget lectus sit amet consequat? Duis eleifend, mi eu pellentesque ultrices, odio eros porttitor elit, sed suscipit nisl risus eget nibh. Nunc at quam at neque luctus porta.
	</p>

	<p>
		Nunc semper ultricies sapien, vel faucibus eros auctor convallis! Ut non enim eu mauris euismod mollis in et urna. Vivamus tincidunt mi ac felis scelerisque, et varius nisi scelerisque! Mauris in luctus ligula, non sollicitudin nisi. Nullam placerat molestie massa? In at luctus magna, ut placerat libero. Nulla sed ante at dui accumsan fermentum. Vestibulum volutpat massa a diam consectetur consequat. In non eleifend metus.
	</p>

	<p>
		Vivamus sit amet malesuada nisi. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam eget urna at metus condimentum tempor quis vitae turpis. Donec non varius enim, non luctus mauris. Phasellus ultrices ultrices aliquam. Proin ac viverra nunc. Nam auctor porta nunc vel euismod. Duis quis eros nibh. In id vulputate orci. Nullam iaculis nunc sed consequat condimentum. Nullam posuere mauris lectus, quis interdum turpis bibendum ac. Pellentesque non mi ut ligula convallis feugiat! Vivamus rhoncus, dui id laoreet placerat, risus ipsum sagittis sem, non eleifend nisi risus et tortor! Sed accumsan facilisis est, quis facilisis sapien ultrices a. Nulla quis condimentum lacus, ac condimentum nibh! Aenean sit amet lorem lorem.
	</p>
</div>

<?php

$body = ob_get_contents();
ob_end_clean();

echo $renderer->render([
	"title" => "Home",
	"body" => $body,
	"css" => $css,
]);

//$_CACHE->finish();

?>
