<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="/static/css/reset.css" type="text/css"/>
		<link rel="stylesheet" href="/static/css/base.css" type="text/css"/>
		
		<link rel="stylesheet" href="/static/fonts/earthbound/font-face.css" type="text/css"/>
		%css%
		<title>Imanust - %title%</title>
	</head>
	<body>
		<div id="container">
			<div class="header">
				<div class="logo">
				</div>
				<div class="slogan">
					<span>
						Ferramenta de lalala
					</span>
				</div>
			</div>
			<div class="content">
				<div class="sidebar">
					<div class="login_area">
						<h1>Acesso</h1>
						<hr/>
						<form>
							<input type="email" placeholder="Email">
							<br/>
							<input type="password" placeholder="Password">
							<br/>
							<input type="submit" value="Entrar">
							<br/>
							<a href="/imanust/forgot_password/">
								Esqueci minha senha
							</a>
							<br/>
							<a href="/imanust/request_access/">
								Solicitar acesso
							</a>
						</form>
					</div>
					<div class="stations_area">
						<h1>Locais</h1>
						<hr/>
						<img src="/static/img/station1.png" class="station_icon"/>
						<a href="/imanust/station/1">
							Esta&ccedil;&atilde;o Primeira de Mangueira - Rio de Janeiro, RJ
						</a>
					</div>
				</div>
				<div class="main_content">
					%body%
				</div>
			</div>
		</div>
		<div id="footer">
			<p>
				Vestibulum porttitor lorem semper, feugiat eros id, congue est. Quisque sed cursus lectus. Vestibulum quis lobortis.
			</p>
			<p>
				In fringilla luctus justo a viverra. Nam nec sollicitudin erat. Integer et laoreet arcu, sit amet.
			</p>
			<p>
				<a href="/imanust">Link A</a> - <a href="/imanust">Link B</a> - <a href="/imanust">Link C</a>
			</p>
		</div>
		<div id="screen">
		</div>
		<div id="hidden">
		</div>
	</body>
</html>

<?php

$default_template_context = [
	"title" => "Base",
	"body" => "",
	"css" => "",
];

?>
